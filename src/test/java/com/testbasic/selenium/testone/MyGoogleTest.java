package com.testbasic.selenium.testone;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyGoogleTest {
		@Test
	    public void startWebDriver(){
			//System.setProperty("webdriver.chrome.driver", "C:\\projects\\chromedriver\\chromedriver.exe");

			WebDriver driver = new ChromeDriver();       
		    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		    driver.navigate().to("http://www.google.com");
		    driver.close();
	        driver.quit();
	    }
}